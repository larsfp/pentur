#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2015-2019 Lars Falk-Petersen <dev@falkp.no>.

system_version = '1.3'
system_name = 'pentur.py'

from tabulate import tabulate
from colors import line_color, colored
from colorama import init, Fore, Style; init() # colorama

import sys # exit
import datetime
import time # verbose
import requests # fetch APIs
import json # read APIs

TransportationType = {
    'street':       '🚏',
    'poi':          '📍',
    'bus':          '🚌',
    'coach':        '🚐',
    'busStation':   '🚍',
    'onstreetBus':  '🚌',
    'ferry':        '⛴',
    'water':        '⛴',
    'ferryStop':    '⛴',
    'rail':         '🚆',
    'railStation':  '🚉',
    'tram':         '🚋',
    'onstreetTram': '🚋',
    'metro':        '🚃',  # 🚇 Ⓣ
    'metroStation': '🚃',  # 🚇 Ⓣ
    'GroupOfStopPlaces': '❇️',
    'airport': '️🛫',
    'air': '️✈️',
    'library': '📖',
    'theatre': '💀',
}

icons = {
    'situation_normal': '❗ ',
    'notice': '⚠️ ',
}

journeyplanner_api = 'https://api.entur.io/journey-planner/v2/graphql'
geocoder_api = 'https://api.entur.io/geocoder/v1/autocomplete?size=10&lang=no&text=' # https://developer.entur.org/pages-geocoder-intro
stops_api = 'https://api.entur.io/stop-places/v1/graphql' # Nasjonalt stoppestedsregister (NSR) https://developer.entur.org/pages-nsr-nsr
client_name = 'gitlab.com/larsfp/pentur'

verbose = False
deviations = True
journey = False # Show journey number (-j)
limitresults = 10
line_number = None # Limit results by line_number (-l)
platform_prefix = None
html = False # Output html for web version #TODO
deviations_width = 40
deviations_length = 110

class columns:
    LINE = 'Linje'
    DESTINATION = 'Destinasjon'
    PLATFORM = 'Plattform'
    OCCUPANCY = 'Full'
    TIME = 'Tid (forsink.)'
    JOURNEY = 'Turnummer'
    DEVIATION = 'Avvik'

    order = [ LINE, DESTINATION, PLATFORM, OCCUPANCY, TIME, JOURNEY, DEVIATION ]

def usage():
    print('Bruk: %s [-a] [-l] [-n] [-v] <stasjonsnavn|stasjonsid>' % sys.argv[0])
    print('''
    -h       Vis denne hjelpen.

    -a       Ikke bruk Unicode symboler/ikoner, kun tekst.
    -c       Deaktiver linjefarger.
    -d       Ikke vis avvik.
    -j       Vis turnummer.
    -l       Begrens treff til kun linje-nummer (kommaseparert).
    -n       Begrens treff pr. platform, tilbakefall er %s.
    -p       Begrens treff til platform-navn (prefix).
    -w       Skriv ut HTML.
    -v       Verbose for utfyllende informasjon.

    %s jernbanetorget
    %s "asker stasjon"
    %s 58277
    ''' % (limitresults, sys.argv[0], sys.argv[0], sys.argv[0]))

    for icon in TransportationType:
        print (icon, TransportationType[icon])

    print(system_name, colored(Fore.BLUE, 'version'), system_version)
    sys.exit(1)

""" Search for stop, return matches as dict """
def fetch_stops(name_needle):
    name_needle = name_needle.lower()
    results = {}
    url = geocoder_api + name_needle

    j = fetch_api_json(url)

    for stop in j['features']:
        categories = ''
        county = ''
        for c in stop['properties']['category']:
            try:
                categories += TransportationType[c] + ' '
            except KeyError as err:
                if verbose:
                    print("Category %s missing icon." % err)

        try:
            county = stop['properties']['county']
        except KeyError: pass
        results[stop['properties']['id']] = stop['properties']['label'] + " (" + county + ", " + categories + ")"

    #TODO: Check for direct hit?
    return results

""" Fetch json from url, return json object """
def fetch_api_json(url):
    html = None
    api_latency = None
    headers =  {"ET-Client-Name": client_name}

    start = time.time()
    try:
        r = requests.get(url, headers = headers)

        if 200 != r.status_code:
            print ("Error fetching geocoder_api %s." % url)
            if verbose:
                print(r.headers)
                print(r.text)
            sys.exit(1)

        html = r.text
    except requests.ConnectionError as error:
        print(url, error)
        sys.exit(1)

    stop = time.time()
    api_latency = stop-start

    if verbose:
        print("%s fetched in %0.3f s" % (url, api_latency))
    return json.loads(html)

""" Fetch data from GQL and return json """
def fetch_api_gql(url, data = None):
    headers =  {"ET-Client-Name": client_name}

    from gql import gql, Client
    from gql.transport.requests import RequestsHTTPTransport
    _transport = RequestsHTTPTransport(url=url, use_json=True, headers=headers)
    client = Client(
        transport=_transport,
        fetch_schema_from_transport=True
    )

    result = None
    api_latency = None

    start = time.time()
    query = gql(data)
    result = client.execute(query)

    stop = time.time()
    api_latency = stop-start

    if verbose: print("%s fetched in %0.3f s" % (url, api_latency))
    return result

""" Look-up stop id based on name """
def get_stopid(stopname):
    """ I.E.: GET https://api.entur.io/geocoder/v1/autocomplete?text=%C3%B8ster%C3%A5s&size=20&lang=no """

    status = None
    messages = ''
    stopid = None

    if verbose: print("Looking up stopname", stopname)

    if is_number(stopname):
        return 'NSR:StopPlace:' + stopname, status, ''

    stops = fetch_stops(stopname)

    if len(stops) > 1:
        messages += "Multiple hits, please use more exact name. Using first hit:\n"
        status = 3
        for key in stops:
            messages += "[%s] \"%s\" \n" % (stops[key], key)
        stopid = list(stops.keys())[0]
        messages += "\nDepartures from %s, updated at %s\n" \
            % (list(stops.values())[0], colored(Style.BRIGHT, datetime.datetime.now().strftime("%H:%M")))

    elif 0 == len(stops):
        messages += "No hits on stop name."
        status = 4
    else: # Direct hit
        stopid = list(stops.keys())[0]
        selected_stop = stops[stopid]
        messages += "Departures from %s, updated at %s\n" \
            % (selected_stop, colored(Style.BRIGHT, datetime.datetime.now().strftime("%H:%M")))
        status = 0

    return stopid, status, messages

""" Fetch departures for stop """
def get_departures(stopid: str):
    """
    https://api.entur.io/journey-planner/v2/ide/?query=%23%20Avgangstavle%20-%20Stavanger%20stadion%0A%0A%7B%0A%20%20stopPlace(id%3A%20%22NSR%3AStopPlace%3A548%22)%20%7B%0A%20%20%20%20id%0A%20%20%20%20name%0A%20%20%20%20estimatedCalls(timeRange%3A%2072100%2C%20numberOfDepartures%3A%2010)%20%7B%20%20%20%20%20%0A%20%20%20%20%20%20realtime%0A%20%20%20%20%20%20aimedArrivalTime%0A%20%20%20%20%20%20aimedDepartureTime%0A%20%20%20%20%20%20expectedArrivalTime%0A%20%20%20%20%20%20expectedDepartureTime%0A%20%20%20%20%20%20actualArrivalTime%0A%20%20%20%20%20%20actualDepartureTime%0A%20%20%20%20%20%20date%0A%20%20%20%20%20%20forBoarding%0A%20%20%20%20%20%20forAlighting%0A%20%20%20%20%20%20destinationDisplay%20%7B%0A%20%20%20%20%20%20%20%20frontText%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20quay%20%7B%0A%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20serviceJourney%20%7B%0A%20%20%20%20%20%20%20%20journeyPattern%20%7B%0A%20%20%20%20%20%20%20%20%20%20line%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20%20%20transportMode%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A
    """
    departures = []
    # TODO: remove any value not needed from query
    departurequery = '{stopPlace(id: "' + stopid + '''") {
            id
            name
            estimatedCalls(timeRange: 72100, numberOfDepartures: 200) {
                realtime
                aimedArrivalTime
                aimedDepartureTime
                expectedArrivalTime
                expectedDepartureTime
                actualArrivalTime
                actualDepartureTime
                date
                forBoarding
                forAlighting
                destinationDisplay {
                    frontText
                }
                quay {
                    id
                    name
                }
                serviceJourney {
                    line {
                        id
                        publicCode
                        name
                        transportMode
                        situations {
                            id
                            situationNumber
                            severity
                            summary {
                                value
                                language
                            }
                            advice {
                                value
                                language
                            }
                            description{
                                value
                                language
                            }
                            validityPeriod{
                                startTime
                                endTime
                            }
                            summary {
                                value
                                language
                            }
                        }
                    }
                    situations {
                        id
                        summary {
                            value
                            language
                        }
                    }
                    notices {
                        id
                        text
                    }
                }
            }
        }
    }
    '''

    j = fetch_api_gql(journeyplanner_api, data = departurequery)

    if None == j['stopPlace']:
        print ("No departures (check stop id, %s)" % stopid)
        return {}

    for estimatedCall in j['stopPlace']['estimatedCalls']:
        departure = {}

        try:
            departure['destinationDisplay'] = estimatedCall['destinationDisplay']['frontText']
        except KeyError: pass
        try:
            departure['lineId'] = estimatedCall['serviceJourney']['line']['publicCode']
        except KeyError: pass
        try:
            departure['quayId'] = estimatedCall['quay']['id']
            departure['DeparturePlatformName'] = estimatedCall['quay']['name'] + ' (%s)' % departure['quayId']
        except KeyError: pass
        try:
            departure['transportMode'] = estimatedCall['serviceJourney']['line']['transportMode']
        except KeyError: pass
        try:
            departure['AimedDepartureTime'] = datetime.datetime.strptime(estimatedCall['aimedDepartureTime'], '%Y-%m-%dT%H:%M:00%z')
        except KeyError: pass
        try:
            departure['ExpectedDepartureTime'] = datetime.datetime.strptime(estimatedCall['expectedDepartureTime'], '%Y-%m-%dT%H:%M:%S%z')
        except KeyError: pass
        try:
            departure['notices'] = estimatedCall['serviceJourney']['notices']
        except KeyError: pass
        try:
            departure['situations'] = estimatedCall['serviceJourney']['line']['situations']
        except KeyError: pass

        departures.append(departure)

    # Sort departures by platform ID
    return sorted(departures, key=lambda k: k['quayId'])

def format_delay(aimed, expected = False):
    if not expected or aimed == expected:
        return ''

    delay = expected.timestamp() - aimed.timestamp()
    return ' (%.0f)' % prettify_seconds(delay)

def prettify_seconds (sec):
    if sec > 3600:
        return '>1h'

    output = 's'
    s = sec%60
    m = sec//60

    if 0 < s:
        output = s + output
    if 0 < m:
        output = m + ':' + output
    return output

''' Filter lines '''
#TODO: rewrite to use quayid instead of DeparturePlatformName
def filter_departures(departures):
    filtered = []
    directions = {}

    for departure in departures:
        # Limit results by line_number
        if line_number:
            if departure['lineId'] not in line_number:
                continue

        # Limit results by platform_prefix
        if platform_prefix:
            if platform_prefix.endswith('$'):
                if departure['DeparturePlatformName'] != platform_prefix[:-1]:
                    continue
            elif not departure['DeparturePlatformName'].startswith(str(platform_prefix)):
                continue

        # Keep list of platforms with number of hits
        try:
            if departure['DeparturePlatformName'] not in directions.keys():
                directions[departure['DeparturePlatformName']] = 1
            else:
                directions[departure['DeparturePlatformName']] += 1
        except TypeError:
            pass

        # Limit hits by platform
        if directions[departure['DeparturePlatformName']] > limitresults:
            continue
        filtered.append(departure)
    return filtered

''' Format as table '''
def to_table(departures):
    result = []
    prev_quayid=None

    for departure in departures:
        row = {}
        delay = ''
        # Add a blank entry when switching platforms
        if prev_quayid and prev_quayid != departure['quayId']:
            result.append({})
        prev_quayid=departure['quayId']

        ### Start formatting

        # Icon, double for long vehicles
        symbol = TransportationType[departure['transportMode']]
        icon = '{:<2.2}'.format(symbol)
               # padding, truncate. https://pyformat.info/

        line = departure['lineId']
        line = line_color(line)

        row[columns.LINE] = icon + line
        row[columns.DESTINATION] = departure['destinationDisplay']
        row[columns.PLATFORM] = departure['DeparturePlatformName']

        try:
            delay = format_delay(departure['AimedDepartureTime'], departure['ExpectedDepartureTime'])
        except TypeError: pass

        # Time as HH:MM[kø] if today
        # if departure['AimedDepartureTime'].day == datetime.date.today().day:
        #     row[columns.TIME] = departure['AimedDepartureTime'].strftime("%H:%M") + \
        #                   ('kø' if 'true' == departure['InCongestion'] else '') + \
        #                   delay
        # else:
        row[columns.TIME] = departure['AimedDepartureTime'].strftime("%H:%M") + delay

        row[columns.DEVIATION] = ''
        # situations
        if 'situations' in departure:
            deviation_formatted = "%s" % icons['situation_normal']
            for situation in departure['situations']:
                # advice [{language: no, value: '...'}]
                # summary: [{lang:no, value: '...'}]

                if not situation['validityPeriod']['endTime'].startswith('9999-'): # API stupidity?
                    if departure['AimedDepartureTime'] > datetime.datetime.strptime(situation['validityPeriod']['endTime'], '%Y-%m-%dT%H:%M:00%z')  or \
                        departure['AimedDepartureTime'] < datetime.datetime.strptime(situation['validityPeriod']['startTime'], '%Y-%m-%dT%H:%M:00%z'):
                        continue

                if 'normal' != situation['severity']:
                    deviation_formatted += colored(Fore.RED, ' ! ')

                deviation_formatted += "%s " % insert_newlines(situation['summary'][0]['value'], every=deviations_width)[:deviations_length]
                row[columns.DEVIATION] += deviation_formatted

        # notices
        if 'notices' in departure and 0 < len(departure['notices']):
             deviation_formatted = "%s" %  icons['notice']
             for notice in departure['notices']:
                 deviation_formatted += "%s " % insert_newlines(notice['text'], every=deviations_width)[:deviations_length]
             row[columns.DEVIATION] += deviation_formatted

        result.append(row)
    return result

''' Helper function to insert newline every n https://stackoverflow.com/questions/2657693/insert-a-newline-character-every-64-characters-using-python'''
def insert_newlines(string, every=64):
    return '\n'.join(string[i:i+every] for i in range(0, len(string), every))

''' Order columns according to columns.order '''
def order_columns(rows):
    # Find all columns with values
    all = [c for r in rows for c in r if r[c] != None]
    keys = [c for c in columns.order if c in all]

    ordered = []
    ordered.append(keys)
    # Retrieve column from each row
    for r in rows:
        ordered.append([r.get(k, None) or '' for k in keys])
    return ordered

''' isnumeric only works in python3 '''
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
 

if __name__ == '__main__':
    stopname=''
    stopid = None

    args = sys.argv[1:]

    if '-h' in args:
        usage()

    if '-v' in args:
        verbose = True
        args.pop(args.index('-v'))
        print(args)

    if '-n' in args:
        limitresults = int(args[ args.index('-n')+1 ])
        args.pop(args.index('-n')+1)
        args.pop(args.index('-n'))
        if verbose:
            print("limitresults", limitresults)
            print(args, stopname)

    if '-l' in args:
        line_number = args[ args.index('-l')+1 ].split(",")
        args.pop(args.index('-l')+1)
        args.pop(args.index('-l'))
        if verbose:
            print("line_number", line_number)

    if '-p' in args:
        platform_prefix = args[ args.index('-p')+1 ]
        args.pop(args.index('-p')+1)
        args.pop(args.index('-p'))
        if verbose:
            print("platform_prefix", platform_prefix)

    if '-P' in args:
        platform_width = int(args[ args.index('-P')+1 ])
        args.pop(args.index('-P')+1)
        args.pop(args.index('-P'))
        if verbose:
            print("platform_width", platform_width)

    if '-d' in args:
        deviations = False
        args.pop(args.index('-d'))

    if '-j' in args:
        journey = True
        args.pop(args.index('-j'))

    if '-c' in args:
        show_colors = False
        args.pop(args.index('-c'))

    if '-w' in args:
        html = True
        args.pop(args.index('-w'))

    if len(args) < 1:
        usage()
    else:
        stopname = ''.join(args[0:])

    format = 'simple'
    if html:
        format = 'html'
        show_colors = False

    stopid, stopid_status, messages = get_stopid(stopname)
    print (messages)
    #if 0 < stopid_status:
    #    sys.exit(stopid_status)

    departures = get_departures(stopid)
    departures = filter_departures(departures)
    print (tabulate(order_columns(to_table(departures)), headers="firstrow", tablefmt=format))
    #latency_string = 'Waited %0.3f s for ruter.no to respond' % api_latency
    #if not html:
    #    print(latency_string)
    #else:
    #    print ("<small>" + latency_string + "</small>")

    sys.exit(0)
