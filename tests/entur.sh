# https://github.com/entur/api-examples/issues/1#issuecomment-509533767

# https://api.entur.io/journey-planner/v2/ide/?query=%23%20Avgangstavle%20-%20Stavanger%20stadion%0A%0A%7B%0A%20%20stopPlace(id%3A%20%22NSR%3AStopPlace%3A58277%22)%20%7B%0A%20%20%20%20id%0A%20%20%20%20name%0A%20%20%20%20estimatedCalls(timeRange%3A%2072100%2C%20numberOfDepartures%3A%2010)%20%7B%20%20%20%20%20%0A%20%20%20%20%20%20realtime%0A%20%20%20%20%20%20aimedArrivalTime%0A%20%20%20%20%20%20aimedDepartureTime%0A%20%20%20%20%20%20expectedArrivalTime%0A%20%20%20%20%20%20expectedDepartureTime%0A%20%20%20%20%20%20actualArrivalTime%0A%20%20%20%20%20%20actualDepartureTime%0A%20%20%20%20%20%20date%0A%20%20%20%20%20%20forBoarding%0A%20%20%20%20%20%20forAlighting%0A%20%20%20%20%20%20destinationDisplay%20%7B%0A%20%20%20%20%20%20%20%20frontText%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20quay%20%7B%0A%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20serviceJourney%20%7B%0A%20%20%20%20%20%20%20%20journeyPattern%20%7B%0A%20%20%20%20%20%20%20%20%20%20line%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%20%20%20%20transportMode%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A

curl 'https://api.entur.io/journey-planner/v2/graphql' \
-H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0' \
-H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' \
--compressed \
-H 'Content-Type: application/json' \
-H 'ET-Client-Name: entur-shamash' \
-H 'X-Correlation-Id: ac3a279f-1b04-4661-89d2-16fa86ca1f05' \
-H 'Origin: https://api.entur.io' \
-H 'DNT: 1' \
-H 'Connection: keep-alive' \
--data '{"query":"# Avgangstavle - Stavanger stadion\n\n{\n  stopPlace(id: \"NSR:StopPlace:58277\") {\n    id\n    name\n    estimatedCalls(timeRange: 72100, numberOfDepartures: 10) {     \n      realtime\n      aimedArrivalTime\n      aimedDepartureTime\n      expectedArrivalTime\n      expectedDepartureTime\n      actualArrivalTime\n      actualDepartureTime\n      date\n      forBoarding\n      forAlighting\n      destinationDisplay {\n        frontText\n      }\n      quay {\n        id\n      }\n      serviceJourney {\n        journeyPattern {\n          line {\n            id\n            name\n            transportMode\n          }\n        }\n      }\n    }\n  }\n}\n","variables":null}'

--data '
{"query":"
    # Avgangstavle - Stavanger stadion\n\n
    {\n  
        stopPlace(id: \"NSR:StopPlace:58277\") {
            id
            name
            estimatedCalls(timeRange: 72100, numberOfDepartures: 10) {
                realtime\n      aimedArrivalTime\n      aimedDepartureTime\n      expectedArrivalTime\n      expectedDepartureTime\n      actualArrivalTime\n      actualDepartureTime\n      date\n      forBoarding\n      forAlighting\n      destinationDisplay {\n        frontText\n      }\n      quay {\n        id\n      }\n      serviceJourney {\n        journeyPattern {\n          line {\n            id\n            name\n            transportMode\n          }\n        }\n      }\n    }\n  }\n}\n","variables":null}'
