# Pentur

A real-time departure terminal client for en-tur.no, Norways unified transportation data. 

This is a continuation of https://gitlab.com/larsfp/rutercli/ for new API.

See INSTALL.md

## License
GNU AFFERO GENERAL PUBLIC LICENSE

## Thanks
* Icons used in web version from https://materialdesignicons.com/
* Thanks to Børge Nordli for lots of patches.
* Thanks to atchoo78 @ github for mac install how-to

## Examples

Output from current version (1.3):

``` bash
$ ./pentur.py majorstuen
Multiple hits, please use more exact name. Using first hit:
[Majorstuen, Oslo (Oslo, 🚌 🚌 🚌 🚌 🚋 🚃 )] "NSR:StopPlace:58381" 
[Majorstuen skole, Oslo (Oslo, 📍 )] "OSM:TopographicPlace:109816448" 
[Majorstuen kirke, Oslo (Oslo, 📍 )] "OSM:TopographicPlace:115921014" 
[Majorstuen legesenter, Oslo (Oslo, 📍 )] "OSM:TopographicPlace:5921085740" 
[ABC legekontor Majorstuen, Oslo (Oslo, 📍 )] "OSM:TopographicPlace:5921085730" 
[Dr. Øien fastlegekontor, Majorstuen, Oslo (Oslo, 📍 )] "OSM:TopographicPlace:5921085735" 

Departures from Majorstuen, Oslo (Oslo, 🚌 🚌 🚌 🚌 🚋 🚃 ), updated at 11:41

Linje    Destinasjon        Plattform    Tid (forsink.)    Avvik
-------  -----------------  -----------  ----------------  -------------------------------------------
🚃  5     Vestli via Tøyen   Majorstuen   11:42
🚃  5     Ringen via Tøyen   Majorstuen   11:46
🚃  2     Ellingsrudåsen     Majorstuen   11:48
🚃  1     Bergkrystallen     Majorstuen   11:44
🚃  4     Bergkrystallen     Majorstuen   11:51
🚃  1     Frognerseteren     Majorstuen   11:43
🚃  5     Ringen via Storo   Majorstuen   11:45
🚃  3     Kolsås             Majorstuen   11:49
🚃  4     Vestli via Storo   Majorstuen   11:51
🚃  2     Østerås            Majorstuen   11:53
🚌  20    Galgeberg          Majorstuen   11:37             ❗ Stopper ikke ved Munchmuseet
🚌  20    Galgeberg          Majorstuen   11:45             ❗ Stopper ikke ved Munchmuseet
🚌  20    Galgeberg          Majorstuen   11:52             ❗ Stopper ikke ved Munchmuseet
🚋  12    Majorstuen         Majorstuen   11:41             ⚠️ Trikken har høyt gulv og trappetrinn, og
                                                            det er dessverre ikke mulig å komme seg
                                                            om bord med rullestol. The
...
```

## TODO

* Test when delays.
* Improve web
* Filter out or mark "forBoarding": false
* Enable or remove the rest of the commented code from previous version
* Add quay,stopPlace,description?
* Situations/notices can be long. Could show ID "for more info ...", but the IDs are insanely long.
* English, Norwegian or multilang?
