#!/usr/bin/env python3

from wsgiref.simple_server import make_server
from cgi import parse_qs, escape
from wsgiref.validate import validator

from tabulate import tabulate
from colorama import init, Fore, Style; init() # colorama

import sys
sys.path.insert(0, '/var/www/graph.no/ruter/')

from colors import line_color, colored
import pentur

stopname=''
html = """
<!DOCTYPE html>
<html lang="no">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Alternativ ruter-klient</title>
		<meta property="og:title" content="Alternativ ruter-klient"/>
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=480, user-scalable=yes" />
		<link rel="apple-touch-icon-precomposed" href="https://static.graph.no/favicon.png" />
		<link rel="Shortcut icon" type="image/x-icon" href="https://static.graph.no/favicon.png"/>
        <link rel="shortcut icon" href="https://static.graph.no/favicon.png" />
</head>
<body style="font-family: monospace;">

<form method="get" action=""><a style="font-size: 3em; text-decoration: none; color: black;" href="https://graph.no/ruter/">#</a> Stopp: <input type="text" name="stopname" value="%s"> <input type="submit" value="Submit">
</form>
%s

<p>
<h3>Examples:</h3>

<ul>
    <li>[<a href="?stopname=58277">58277</a>] "Østerås"</li>
    <li>[<a href="?stopname=majorstuen">majorstuen</a>] "majorstuen"</li>
</ul>
</p>

</body>
</html>
"""

def application(environ, start_response):
    ruteroutput=''

    # Returns a dictionary containing lists as values.
    d = parse_qs(environ['QUERY_STRING'])

    # In this idiom you must issue a list containing a default value.
    stopname = d.get('stopname', [''])[0] # Returns the first value.

    # Always escape user input to avoid script injection
    stopname = escape(stopname)

    if 0 < len(stopname):
        stopid, status, messages = pentur.get_stopid(stopname)
        if stopid:
            departures = pentur.get_departures(stopid)
            ruteroutput = tabulate(pentur.order_columns(pentur.to_table(departures)), headers="firstrow", tablefmt='html')

    response_body = html % (stopname or '', ruteroutput)

    status = '200 OK'

    # Now content type is text/html
    response_headers = [('Content-Type', 'text/html; charset=utf-8'),
                      ('Content-Length', str(len(response_body)+1000))]
    start_response(status, response_headers)

    return [response_body.encode("utf-8")]
